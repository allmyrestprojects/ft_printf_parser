//
// Created by Bears Gerda on 11/01/2020.
//
#include "ft_printf.h"

void    sign_exp_ld(long double x, t_double *num)
{
    int             i;
    int             j;
    int             k;
    unsigned char   *gg;
    unsigned char   tmp;

    gg = (unsigned char *)&x;
    k = 0;
    i = -1;
    while(++i < 10)
    {
        tmp = *(gg++);
        j = -1;
        while (++j < 8)
        {
            if (i >= 8 && (tmp & 1))
                num->e |= (1 << k++);
            else if (i >= 8 && !(tmp & 1))
                num->e &= ~(1 << k++);
            tmp >>= 1;
        }
    }
    num->s = (num->e >> 15) ? -1 : 1;
    num->e = num->e & 0x7fff;
}

void    get_ldouble(long double num, t_double *var)
{
    var->counter = 63;
    var->shift = 16382;
    sign_exp_ld(num, var);
    u_ld.ldl = num;
    var->m = u_ld.ldw & 0xFFFFFFFFFFFFFFFF;
    var->pow = var->e - var->shift - 1;
    var->lshift = 1;
}