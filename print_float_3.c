//
// Created by Bears Gerda on 24/11/2019.
//
#include "ft_printf.h"


void    get_double(double num, t_double *var)
{
    u_d.dl = num;

    var->counter = 51;
    var->shift = 1023;
    var->s = ( u_d.dw >> 63 ) ? -1 : 1;
    var->e = ( u_d.dw >> 52 ) & 0x7FF;
    var->m = var->e ? ( u_d.dw & 0xFFFFFFFFFFFFF ) | 0x10000000000000 : ( u_d.dw & 0xFFFFFFFFFFFFF ) << 1;
    var->pow = var->e - var->shift;
    var->lshift = 0;
}

void    print_wpart(char *str, int fd)
{
    if (*(++str))
        print_wpart(str, fd);
    uputchar_fd(*(--str), fd, 0);
}

void    print_fpart(char *str, int i, int prec, int fd)
{
    if (*(++str))
        print_fpart(str, ++i, prec, fd);
    if (i > prec)
        uputchar_fd(*(--str), fd, 0);
}

char    *rounding(char *num1, char *num2, char *res, int prec)
{
    int tmp;

    tmp = prec;
    ft_strclr(num2);
    ft_strclr(res);
    while(--prec >= 0)
        num2[prec] = '0';
    num2[0] = '1';
    fill_zeros(num1, num2, res);
    add(num1, num2, res);
    if (ft_strlen(res) > ft_strlen(num1) || tmp == 0)
    {
        ft_strncpy(num1, res, ft_strlen(num1));
        return ("1");
    }
    else
        ft_strncpy(num1, res, ft_strlen(res) + 1);
    return ("0");
}

void    sel_wpart(t_double var, t_num num)
{
    if (var.f_flag == 0)
    {
        uputchar_fd('0', var.fd, 0);
        if (num.precision != 0 || num.grate == '#')
            uputchar_fd('.', var.fd, 0);
    }
    else if (var.f_flag == -1)
    {
        print_wpart(num.num3, var.fd);
        if (num.precision != 0 || num.grate == '#')
            uputchar_fd('.', var.fd, 0);
    }
}

int     preoutput2(t_double *var, t_num *num)
{
    int tmp;
    int j;
    char res[BUFF];
    char *carry;

    j = 0;
    carry = "0";
    if (num->precision == 1 || num->precision == 0)
    {
        if ((num->num1[ft_strlen(num->num1) - (1 + num->precision)] - '0') >= 5)
            carry = rounding(num->num1, num->num2, res, num->precision);
        ft_strclr(res);
        add(num->num3, carry, res);
        ft_strncpy(num->num3, res, ft_strlen(num->num1) + 1);
        var->w_flag = 0;
        j = num->precision + 1;
    }
    else if((tmp = (int)ft_strlen(num->num1) - num->precision) > 0)
    {
        if ((num->num1[tmp - 1] - '0') >= 5)
            carry = rounding(num->num1, num->num2, res, num->precision);
        ft_strclr(res);
        add(num->num3, carry, res);
        ft_strncpy(num->num3, res, ft_strlen(num->num1) + 1);
        var->w_flag = 1;
        num->len = tmp;
        j = num->precision + 1;
    }
    else if ((tmp = (int)ft_strlen(num->num1) - num->precision) <= 0)
    {
        var->w_flag = -1;
        num->len = tmp;
        j = tmp + num->precision;
    }
    return (j);
}

void    output(t_double var, t_num num)
{
    int i;

    i = 0;
    if (var.w_flag == 0)
    {
        sel_wpart(var, num);
        if (num.precision != 0)
            uputchar_fd(num.num1[ft_strlen(num.num1) - 1], var.fd, 0);
    }
    else if(var.w_flag == 1)
    {
        sel_wpart(var, num);
        print_fpart(num.num1, i, num.len, var.fd);
    }
    else if (var.w_flag == -1)
    {
        sel_wpart(var, num);
        if (num.num1[0] != '\0')
            print_fpart(num.num1, i, num.len, var.fd);
        while (num.len++ < 0)
            uputchar_fd('0', var.fd, 0);
    }
}

void    add_aux(char *num1, char *num2, int counter, char *dig)
{
    char tmp[BUFF];

    ft_strclr(tmp);
    if (counter < 0)
    {
        get_mod(counter, num2, dig);
        fill_zeros(num1, num2, tmp);
        add(num1, num2, tmp);
        ft_strncpy(num1, tmp, ft_strlen(tmp) + 1);
    }
    else if (counter >= 0)
    {
        get_mod(counter, num1, dig);
        add(num1, num2, tmp);
        ft_strncpy(num2, tmp, ft_strlen(tmp) + 1);
    }
}

void    mod_aux(int counter, char *num1, char *dig, int *flag)
{
    get_mod(counter, num1, dig);
    *flag = counter;
}

void    pow_add(t_double *var, t_num *num)
{
    while(var->m / 2 != 0)
    {
        if (var->m % 2 == 1 && var->counter >= var->e - var->shift)
        {
            var->power = var->counter - (var->e - var->shift) + 1;
            if (var->f_flag == 0)
                mod_aux(-var->power, num->num1, "5", &var->f_flag);
            else
                add_aux(num->num1, num->num2, -var->power, "5");
        }
        else if (var->m % 2 == 1 && var->counter != var->e - var->shift)
        {
            var->power = -(var->counter - (var->e - var->shift) + 1);
            if (var->w_flag == -1)
                mod_aux(var->power, num->num3, "2", &var->w_flag);
            else
                add_aux(num->num4, num->num3, var->power, "2");
        }
        var->m = var->m / 2;
        var->counter--;
    }
}

int     preoutput(t_double *var, t_num *num)
{
    int j;

    j = 0;
    if (var->e - var->shift < 0 || (var->e - var->shift == 0 && var->shift == 16382))
    {
        var->f_flag = 0;
        var->power = -(var->e - var->shift - var->lshift);
        add_aux(num->num1, num->num2, -var->power, "5");
        j = j + 1;
    }
    else
    {
        var->f_flag = -1;
        var->power = var->pow;
        add_aux(num->num4, num->num3, var->power, "2");
        j = j + (int)ft_strlen(num->num3);
    }
    if (num->num1[0] == 0 && num->num2[0] == 0)
        num->num1[0] = '0';
    return(j);
}

void    print_eg(t_double var, t_num num, int len, char sign)
{
    output(var, num);
    uputchar_fd(var.sp, var.fd, 0);
    uputchar_fd(sign, var.fd, 0);
    if (len / 10 == 0)
        uputchar_fd('0', var.fd, 0);
    ft_putnbr(len);
}

void    pos_pow(t_num *num)
{
    ft_strncat(num->num1, num->num3, ft_strlen(num->num3) - 1);
    num->num3[0] = num->num3[ft_strlen(num->num3) - 1];
    num->num3[1] = '\0';
}

void    neg_pow(t_num *num, int len)
{
    num->num3[0] = num->num1[len];
    num->num3[1] = '\0';
    num->num1[len] = '\0';
}

int    output_e(t_double var, t_num num)
{
    int len2;
    int j;
    int len1;

    if (var.pow >= 0)
    {
        len2 = ft_strlen(num.num3);
        pos_pow(&num);
        j = preoutput2(&var, &num);
        print_eg(var, num, len2 - 1, '+');
    }
    else
    {
        len1 = 0;
        len2 = (int)ft_strlen(num.num1) - 1;
        while (num.num1[len2 - len1] == '0')
            len1++;
        neg_pow(&num, len2 - len1);
        var.f_flag = -1;
        j = preoutput2(&var, &num);
        print_eg(var, num, len1 + 1, '-');
    }
    return (j);
}

int    print_g_pos(t_double var, t_num num)
{
    int len2;
    int j;

    if ((len2 = ft_strlen(num.num3)) <= num.precision)
    {
        if (num.precision > ft_strlen(num.num1))
            num.precision = ft_strlen(num.num1) - 1;
        else if (ft_strlen(num.num1) >= num.precision)
            num.precision = num.precision - len2;
        j = preoutput2(&var, &num);
        output(var, num);
    }
    else
    {
        if (num.precision != 0)
            num.precision--;
        pos_pow(&num);
        j = preoutput2(&var, &num);
        var.sp = var.sp - 2;
        print_eg(var, num, len2 - 1, '+');
    }
    return(j);
}

int     print_g_neg(t_double var, t_num num, int len1, int len2)
{
    int j;

    if (len1 > 3)
    {
        if (num.precision > len2 - len1)
            num.precision = len2 - len1;
        neg_pow(&num, len2-len1);
        var.f_flag = -1;
        j = j + preoutput2(&var, &num);
        var.sp = var.sp - 2;
        print_eg(var, num, len1 + 1, '-');
    }
    else
        {
        if (num.precision > len2 - len1)
            num.precision = len2 - len1;
        else
            num.precision++;
        j = j + preoutput2(&var, &num);
        output(var, num);
    }
    return (j);
}

int     output_g(t_double var, t_num num)
{
    int     len1;
    int     len2;
    int     j;

    if (var.pow >= 0)
        j =  print_g_pos(var, num);
    else if (var.pow < 0)
    {
        if (num.precision != 0)
            num.precision--;
        len1 = 0;
        len2 = (int)ft_strlen(num.num1) - 1;
        while (num.num1[len2 - len1] == '0')
            len1++;
        j =  print_g_neg(var, num, len1, len2);
    }
    return (j);
}

void    init(t_double *var, t_num *num ,int flags[])
{
    ft_strclr(num->num1);
    ft_strclr(num->num2);
    ft_strclr(num->num3);
    ft_strclr(num->num4);
    var->f_flag = 0;
    var->w_flag = -1;
    var->fd = flags[12];
    num->precision = flags[7];
    num->grate = flags[3];
}

int     print_double(t_double var, int flags[])
{
    int     j;
    t_num   num;

    init(&var, &num, flags);
    pow_add(&var, &num);
    j = preoutput(&var, &num);
    if (var.sp == 'f')
    {
        j = j + preoutput2(&var, &num);
        output(var, num);
    }
    if (var.sp == 'e' || var.sp == 'E')
        j = j + output_e(var, num);
    if (var.sp == 'g' || var.sp == 'G')
        j = j + output_g(var, num);
    return (j);
}