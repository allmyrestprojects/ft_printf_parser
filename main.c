//
// Created by Bears Gerda on 06/01/2020.
//

#include "ft_printf.h"
#include <stdio.h>
#include <locale.h>

int main(void)
{
    int         i;
    double      x;
    long double y;



    /*
    x = 1.56512341236;

    //setlocale(LC_ALL, "");
    //printf("%S\n", L"🤗 💯 🌍 🚀 🔴 ");

    ft_printf("%s\n", "🤗 💯 🌍 🚀 🔴 Ж ö € § ");
    i = printf("%0.100lf", x);
    printf("\n%d\n", i);
    i = ft_printf("%0.100lf", x);
    printf("\n%d\n", i);


    i = printf("%0.100lf", 34.5e+31);
    printf("\n%d\n", i);
    i = ft_printf("%.100lf", 34.5e+31);
    printf("\n%d\n", i);

    i = printf("%0.1000lf", DBL_MAX);
    printf("\n%d\n", i);
    i = ft_printf("%0.1000lf", DBL_MAX);
    printf("\n%d\n", i);
    i = printf("%0.1000lf", 0.000000000000000000000000000000000002134213);
    printf("\n%d\n", i);
    i = ft_printf("%0.1000lf", 0.000000000000000000000000000000000002134213);
    printf("\n%d\n", i);

    //print_utf();

    y = -123.456231l;
    i = ft_printf("\n%.100Lf", y);
    printf("\n%d\n", i);
    i = printf("\n%.100Lf", y);
    printf("\n%d\n", i);

    printf("\n{%f}{%lf}{%Lf}\n", 1.42, 1.42, 1.42l);
    ft_printf("\n{%f}{%lf}{%Lf}\n", 1.42, 1.42, 1.42l);
    printf("\n{%f}{%lf}{%Lf}\n", -1.42, -1.42, -1.42l);
    ft_printf("\n{%f}{%lf}{%Lf}\n", -1.42, -1.42, -1.42l);
    printf("\n{%f}{%lf}{%Lf}\n", 1444565444646.6465424242242, 1444565444646.6465424242242, 1444565444646.6465424242242l);
    ft_printf("\n{%f}{%lf}{%Lf}\n", 1444565444646.6465424242242, 1444565444646.6465424242242, 1444565444646.6465424242242l);
    printf("\n{%f}{%lf}{%Lf}\n", -1444565444646.6465424242242454654, -1444565444646.6465424242242454654, -1444565444646.6465424242242454654l);
    ft_printf("\n{%f}{%lf}{%Lf}\n", -1444565444646.6465424242242454654, -1444565444646.6465424242242454654, -1444565444646.6465424242242454654l);
    i = printf("%0.1000Lf", LDBL_MAX);
    printf("\n%d\n", i);


    i = ft_printf("%0.1000Lf", LDBL_MAX);
    printf("\n%d\n", i);
    i = printf("%0.1000Lf", LDBL_MAX);
    printf("\n%d\n", i);

    i = ft_printf("%0.17000Lf", LDBL_MAX);

    printf("\n%d\n", i);
    i = printf("%0.17000Lf", LDBL_MIN);
    printf("\n%d\n", i);

    ft_printf("%.100Lf", 0.00000039l);
    printf("\n%.100Lf\n", 0.00000039l);
    ft_printf("\n%Lf", -5.9999999l);
    */

    ft_printf("%.10Le", 1.132423e-40l);
    printf("\n%.10Le\n", 1.132423e-40l);

    ft_printf("\n%.5lE", 234121.132423);
    printf("\n%.5lE", 234121.132423);

    printf("\n\n%lE\n", 123.002341e-123);
    ft_printf("%lE\n",  123.002341e-123);

    printf("\n\n%le\n", 123.054602341e+13);
    ft_printf("%le\n",  123.054602341e+13);

    printf("\n\n%.10le\n", 123.054602341);
    ft_printf("%.10le\n",  123.054602341);

    printf("\n\n%.50le\n", 9023.0546e-34);
    ft_printf("%.50le\n",  9023.0546e-34);

    printf("\n\n%.50le\n", 0.0546e-34);
    ft_printf("%.50le\n",  0.0546e-34);

    printf("\n\n%.50le\n", 0.061e+34);
    ft_printf("%.50le\n",  0.061e+34);

    printf("\n\n%.1le\n", 0.06123e+1);
    ft_printf("%.1le\n",  0.06123e+1);


    printf("\n\n%lG\n", 123.002341e-123);
    ft_printf("%lG\n",  123.002341e-123);

    printf("\n\n%lG\n", 123.054602341e+13);
    ft_printf("%lG\n",  123.054602341e+13);

    printf("\n\n%.10lG\n", 123.054602341);
    ft_printf("%.10lG\n",  123.054602341);

    printf("\n\n%.50lG\n", 9023.0546e-34);
    ft_printf("%.50lG\n",  9023.0546e-34);

    printf("\n\n%.50lG\n", 0.0546e-34);
    ft_printf("%.50lG\n",  0.0546e-34);

    printf("\n\n%.50lG\n", 0.061e+34);
    ft_printf("%.50lG\n",  0.061e+34);

    printf("\n\n%.1lG\n", 0.06123e+1);
    ft_printf("%.1lG\n",  0.06123e+1);


    //print_utf();
    i = -1;
    printf("\n");
    setlocale(LC_ALL, "");
    ft_printf("%s\n", "🤗 💯 🌍 🚀 🔴 Ж ö € § ");
    char s[33];

    while (++i, i < 32)
        s[i] = i + 1;
    s[32] = '\0';
    ft_printf("%r", s);
    return(0);
}